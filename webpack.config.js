// 引入一个包
const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

// webpack中所有的配置信息都应该写在module.exports中
module.exports = {
    // 指定入口文件
    entry: './src/index.ts',
    mode: 'production',
//  指定打包文件所在目录
    output: {
        // 指定打包文件的目录
        path: path.resolve(__dirname, 'dist'),

        //  打包后文件的文件
        filename: 'bundle.js',

        // 告诉webpack不使用箭头
        environment: {
            arrowFunction: false,
            const: false
        }
    },
    //  指定webpack打包时要使用模板
    module: {
        //   指定要加载的规则
        rules: [
            {
                // test指定阿是规则生效的文件
                test: /\.ts$/,          //指定以ts结尾的文件
                use: [
                    {
                        // 指定加载器
                        loader: 'babel-loader',      // 要使用的loader
                        // 设置 babel
                        options: {
                            //    设置预定义的环境
                            presets: [
                                [
                                    //  指定环境的插件
                                    "@babel/preset-env",
                                    // 配置信息
                                    {
                                        // 要兼容的浏览器
                                        targets: {
                                            "chrome": "88"
                                        },
                                        // 指定corejs的版本
                                        "corejs": "3",
                                        // 使用corejs的方式 ”usage” 表示按需加载
                                        "useBuiltIns": "usage"
                                    }
                                ]
                            ]
                        }
                    },
                    'ts-loader',        //用ts-loader
                ],
                //    要排除的文件
                exclude: /node_modules/
            },
            // 设置指定的less文件的处理
            {
                test: /\.less$/,
                use: [
                    "style-loader",
                    "css-loader",
                    // 引入postcss
                    {
                        loader: "postcss-loader",
                        options: {
                            postcssOptions: {
                                plugins: [
                                    [
                                        "postcss-preset-env",
                                        {
                                            browsers: "last 2 versions"
                                        }
                                    ]
                                ]
                            }
                        }
                    },
                    "less-loader",
                ]
            }
        ]
    },
    //  配置webpack插件
    plugins: [
        new CleanWebpackPlugin(),
        new HTMLWebpackPlugin({
            // title:'自定义的title',
            template: './src/index.html'
        })
    ],
    // 用来设置引用模块
    resolve: {
        extensions: ['.ts', '.js']
    }
};

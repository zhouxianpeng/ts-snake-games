// 定义食物类
class Food {
    element: HTMLElement;

    constructor() {
        this.element = document.getElementById("food")!;
    }

    // 设置食物的坐标位置
    get x() {
        return this.element.offsetLeft;
    }

    get y() {
        return this.element.offsetTop;
    }

    // 定义修改食物的位置
    change() {
        // 定义食物的随机位置
        let top = Math.round(Math.random() * 29) * 10;
        let left = Math.round(Math.random() * 29) * 10;

        this.element.style.top = top + 'px';
        this.element.style.left = left + 'px';
    }
}
export default Food;

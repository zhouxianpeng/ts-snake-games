// 定义蛇类
class Snake {
    // 蛇的头部和身体
    head: HTMLElement;
    bodies: HTMLCollection;

    // 获取蛇的容器
    element: HTMLElement;

    constructor() {
        this.element = document.getElementById("snake")!;
        this.head = document.querySelector('#snake > div') as HTMLElement;
        this.bodies = this.element.getElementsByTagName('div');
    }

    // 获取蛇头的坐标
    get X() {
        return this.head.offsetLeft;
    }

    get Y() {
        return this.head.offsetTop;
    }

    // 设置蛇头的坐标
    set X(value) {
        // 如新值和旧值相同，则直接返回
        if (this.X === value) {
            return;
        }
        //  X Y的合法范围0~290之间
        if (value < 0 || value > 290) {
            throw new Error('蛇撞墙啦！！！');
        }
        // 修改X时是在修改水平坐标 蛇在左右移动 向左移动时不可以直接转向右
        if (this.bodies[1] && (this.bodies[1] as HTMLElement).offsetLeft === value) {
            // console.log('水平掉头');
            //  如果发生了掉头，让蛇向反方向继续移动
            if (value > this.X) {
                value = this.X - 10;
            } else {
                value = this.X + 10;
            }
        }

        // 移动身体
        this.moveBody();
        this.head.style.left = value + 'px';
        this.checkHeadBody();
    }

    set Y(value) {
        if (this.Y === value) {
            return;
        }
        if (value < 0 || value > 290) {
            throw new Error('蛇撞墙啦！！！');
        }

        // 修改Y时是在修改垂直坐标 蛇在上下移动 向上移动时不可以直接向下 反之亦然
        if (this.bodies[1] && (this.bodies[1] as HTMLElement).offsetTop === value) {
            // console.log('水平掉头');
            //  如果发生了掉头，让蛇向反方向继续移动
            if (value > this.Y) {
                value = this.Y - 10;
            } else {
                value = this.Y + 10;
            }
        }
        // 移动身体
        this.moveBody();
        this.head.style.top = value + 'px';
        this.checkHeadBody()
    }

    // 蛇增加身体的方法
    addBody() {
        // 向element中添加一个div
        // this.element.insertAdjacentHTML("beforeend", "<div><div/>");
        let newBody = document.createElement('div')
        this.element.appendChild(newBody)
    }

    // 蛇身体移动的方法
    moveBody() {
        for (let i = this.bodies.length - 1; i > 0; i--) {
            // 获取前边身体的位置
            let X = (this.bodies[i - 1] as HTMLElement).offsetLeft;
            let Y = (this.bodies[i - 1] as HTMLElement).offsetTop;

            // 将值设置到身体上
            (this.bodies[i] as HTMLElement).style.left = X + 'px';
            (this.bodies[i] as HTMLElement).style.top = Y + 'px';
        }
    }

    // 检查头和身体是否相撞
    checkHeadBody() {
        for (let i = 1; i < this.bodies.length; i++) {
            let db = this.bodies[i] as HTMLElement;
            if (this.X === db.offsetLeft && this.Y === db.offsetTop) {
                throw new Error('吃到自己啦！！！');
            }
        }
    }
}

export default Snake;

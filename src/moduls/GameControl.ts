// 引入其他类
import Food from "./Food";
import Snake from "./Snake";
import ScorePanel from "./ScorePanel";

// 游戏控制器 控制其它类
class GameControl {
    // 定义三个属性：食物，蛇，记分牌
    food: Food;
    snake: Snake;
    scorePanel: ScorePanel;

    //用来存储蛇的移动方向
    direction: string = "";

    // 记录游戏是否结束
    isLive = true;

    constructor() {
        this.food = new Food();
        this.snake = new Snake();
        this.scorePanel = new ScorePanel(10,2);
        this.init();
    }

    // 游戏初始化方法，调用即开始
    init() {
        // 绑定键盘按下事件
        document.addEventListener('keydown', this.keydownHandler.bind(this));
        // 调用run方法，蛇移动
        this.run();
    }

    // 键盘按下响应函数
    keydownHandler(event: KeyboardEvent) {
        // 修改direction属性
        this.direction = event.key;
    }

    // 蛇移动的方法
    run() {
        // 获取蛇的坐标
        let X = this.snake.X;
        let Y = this.snake.Y;

        // 根据按键方向修改X，Y的值
        switch (this.direction) {
            case 'ArrowUp':
            case 'Up':
                //  向上移动 top 减少
                Y -= 10;
                break;
            case 'ArrowDown':
            case 'Down':
                //  向下移动 top 增加
                Y += 10;
                break;
            case 'ArrowLeft':
            case 'Left':
                //  向左移动 left 减少
                X -= 10;
                break;
            case 'ArrowRight':
            case 'Right':
                //   向左移动 left 增加
                X += 10;
                break;
        }

        // 检测蛇是否吃到食物
        this.checkEat(X, Y)

        try {
            //  修改蛇的X和Y值
            this.snake.X = X;
            this.snake.Y = Y;
        } catch ({message}) {
            alert(message + 'GAME OVER!');
            this.isLive = false;
        }

        // 开启定时调用
        this.isLive && setTimeout(this.run.bind(this), 250 - (this.scorePanel.level - 1) * 30);
    }

    // 定义蛇是否迟到食物方法
    checkEat(X: number, Y: number) {
        if (X === this.food.x && Y === this.food.y) {
            // 食物位置重置
            this.food.change();
            // 分数增加
            this.scorePanel.addScore();
            // 蛇长一截
            this.snake.addBody();
        }
    }
}

export default GameControl;
